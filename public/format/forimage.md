## 画像の取扱い方
diagrams.net
こちらでの編集を提案します

<img src="../images/01_toppage.png"
    style="border: 1px #ccc solid;width:70%">
- 編集途中の画像ファイルを後から編集可能な形式で保存できる（.drawio形式）
  その後、jpg,pdfなど様々な形式で出力可能
- 編集が必要な場合はdrawio形式のファイルを開き追加できるので一から作成するよりは楽
- OS・ソフトに依存することがない（Excel,PowerPoint使わない）、ブラウザ表示できればOK
