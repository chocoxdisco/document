# Mattermostの使い方1
## Mattermostの使い方2
### Mattermostの使い方3
#### Mattermostの使い方4
##### Mattermostの使い方5
###### Mattermostの使い方6

- Mattermostの使い方1
+ Mattermostの使い方2
 - マタモスの使い方2-1
 + マタモスの使い方2-2

---
***

- [x] Mattermostの使い方
- [x] Mattermostの使い方

*Mattermost*の使い方
**Mattermost**の使い方
~~Mattermost~~の使い方
`Mattermost`の使い方

[Mattermostの使い方](https://ixmark.jp/update/437/)

```
Mattermostの使い方
```

|Mattermost|Mattermost|Mattermost|
|:--|:--:|--:|
|マタモス|マタモス|マタモス|

# 見出し1
## 見出し2
### 見出し3
#### 見出し4
##### 見出し5
###### 見出し6

- リスト1
    - ネスト リスト1_1
        - ネスト リスト1_1_1
        - ネスト リスト1_1_2
    - ネスト リスト1_2
- リスト2
- リスト3

1. 番号付きリスト1
    1. 番号付きリスト1_1
    1. 番号付きリスト1_2
1. 番号付きリスト2
1. 番号付きリスト3


> お世話になります。xxxです。
> 
> ご連絡いただいた、バグの件ですが、仕様です。

    # Tab
    class Hoge
        def hoge
            print 'hoge'
        end
    end

インストールコマンドは `gem install hoge` です

normal *italic* normal
normal _italic_ normal

normal **bold** normal
normal __bold__ normal

normal ***bold*** normal
normal ___bold___ normal

***

___

---

*    *    *

[こっちからgoogle][google]
その他の文章
[こっちからもgoogle][google]

[google]: https://www.google.co.jp/

[Google先生](https://www.google.co.jp/)

https://www.google.co.jp/

~~取り消し線~~

~~~
　class Hoge
　  def hoge
　    print 'hoge'
　  end
　end
~~~

|header1|header2|header3|
|:--|--:|:--:|
|align left|align right|align center|
|a|b|c|

| a   | b     | c      | d                  |
|-----|-------|--------|--------------------|
| aaa | aaaaa | あああ | あああああああああ |
| aaa | aaaaa | あああ | あああああああああ |
| aaa | aaaaa | あああ | あああああああああ |
| aaa | aaaaa | あああ | あああああああああ |
| aaa | aaaaa | あああ | あああああああああ |
| aaa | aaaaa | あああ | あああああああああ |

|![picture 1](./images/01_toppage.png)  
--|

- [画像の取扱い方](format/forimage.md)


